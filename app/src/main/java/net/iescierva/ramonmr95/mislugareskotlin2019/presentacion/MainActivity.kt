package net.iescierva.ramonmr95.mislugareskotlin2019.presentacion

import android.os.Bundle

import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import android.view.View
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText


import net.iescierva.ramonmr95.mislugareskotlin2019.R
import net.iescierva.ramonmr95.mislugareskotlin2019.casos_uso.CasosUsoLugar
import net.iescierva.ramonmr95.mislugareskotlin2019.Aplicacion
import net.iescierva.ramonmr95.mislugareskotlin2019.casos_uso.CasosUsoActividades

class MainActivity : AppCompatActivity() {

    private val adaptador by lazy { (application as Aplicacion).adaptador }
    private val casosUsoLugar by lazy { CasosUsoLugar(this, lugares) }
    private val lugares by lazy { (application as Aplicacion).lugares }
    private lateinit var casosUsoActividades : CasosUsoActividades

    private lateinit var recyclerView: RecyclerView
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var separador: RecyclerView.ItemDecoration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        casosUsoActividades = CasosUsoActividades(this)
        addToolbarAndFloatButton()
        createRecycleView()
    }

    override fun onResume() {
        super.onResume()
        adaptador.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_settings) {
            casosUsoActividades.lanzarPreferencias()
            return true
        }

        if (id == R.id.acercaDe) {
            casosUsoActividades.lanzarAcercaDe()
            return true
        }

        if (id == R.id.menu_buscar) {
            lanzarVistaLugar(null)
            return true
        }
        return super.onOptionsItemSelected(item)
    }


    fun lanzarVistaLugar(view: View?) {
        val entrada = EditText(this)
        entrada.setText("0")
        AlertDialog.Builder(this)
                .setTitle(R.string.vistalugar_lanzar)
                .setMessage(R.string.vistalugar_lanzar_id)
                .setView(entrada)
                .setPositiveButton(R.string.vistalugar_lanzar_ok) { dialog, whichButton ->
                    val id = Integer.parseInt(entrada.text.toString())
                    casosUsoLugar.mostrar(id)
                }
                .setNegativeButton(R.string.vistalugar_lanzar_cancelar, null)
                .show()
    }


    fun cerrar(view: View) {
        finish()
    }

    fun createRecycleView() {
        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.adapter = adaptador
        layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        separador = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        recyclerView.addItemDecoration(separador)
//        adaptador.setOnItemClickListener { v ->
//            val pos = recyclerView.getChildAdapterPosition(v)
//            casosUsoLugar.mostrar(pos)
//        }
    }

    fun addToolbarAndFloatButton() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }
}
