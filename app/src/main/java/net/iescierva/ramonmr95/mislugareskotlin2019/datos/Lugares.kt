package net.iescierva.ramonmr95.mislugareskotlin2019.datos

import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.Lugar

interface Lugares {
    fun elemento(id: Int): Lugar  //Devuelve el elemento dado su id
    fun anyade(lugar: Lugar)  //Añade el elemento indicado
    fun nuevo(): Int  //Añade un elemento en blanco y devuelve su id
    fun borrar(id: Int)  //Elimina el elemento con el id indicado
    fun tamanyo(): Int  //Devuelve el número de elementos
    fun actualiza(id: Int, lugar: Lugar)  //Reemplaza un elemento
}