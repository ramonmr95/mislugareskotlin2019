package net.iescierva.ramonmr95.mislugareskotlin2019.modelo


import net.iescierva.ramonmr95.mislugareskotlin2019.datos.GeoException
import kotlin.math.pow
import kotlin.math.sqrt

class GeoPuntoAlt(latitud: Double, longitud: Double, private var altitud: Double) : GeoPunto(latitud, longitud) {

    fun distancia(punto: GeoPuntoAlt): Double {
        val distancia = super.distancia(punto)
        return sqrt(distancia.pow(2.0) + (punto.altitud - altitud).pow(2.0))
    }

    fun getAltitud(): Double {
        return altitud
    }

    @Throws(GeoException::class)
    fun setAltitud(altitud: Double) {
        if (isAltitudOk(altitud)) {
            this.altitud = altitud
        } else throw GeoException("GeoPuntoAlt.setAltitud(double): {$altitud}")
    }

    @Throws(GeoException::class)
    fun isAltitudOk(altitud: Double): Boolean {
        return altitud in ALTITUD_MAXIMA..ALTITUD_MINIMA
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GeoPuntoAlt) return false
        if (!super.equals(other)) return false
        return other.getAltitud().compareTo(getAltitud()) == 0
    }

    override fun toString(): String {
        return super.toString() + ", Altitud: " + this.altitud
    }

    override fun hashCode(): Int {
        var result = super.hashCode()
        result = 31 * result + altitud.hashCode()
        return result
    }

    companion object {
        const val ALTITUD_MAXIMA = 9000.0
        const val ALTITUD_MINIMA = -500.0
    }

}