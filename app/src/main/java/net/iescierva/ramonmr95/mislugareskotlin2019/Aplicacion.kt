package net.iescierva.ramonmr95.mislugareskotlin2019

import android.app.Application

import net.iescierva.ramonmr95.mislugareskotlin2019.datos.Lugares
import net.iescierva.ramonmr95.mislugareskotlin2019.datos.LugaresLista
import net.iescierva.ramonmr95.mislugareskotlin2019.presentacion.AdaptadorLugares

class Aplicacion : Application() {

    var lugares: Lugares
    var adaptador: AdaptadorLugares

    init {
        lugares = LugaresLista()
        adaptador = AdaptadorLugares(lugares)
    }

    override fun onCreate() {
        super.onCreate()
    }
}