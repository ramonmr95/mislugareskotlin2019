package net.iescierva.ramonmr95.mislugareskotlin2019.presentacion

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity

import net.iescierva.ramonmr95.mislugareskotlin2019.Aplicacion
import net.iescierva.ramonmr95.mislugareskotlin2019.R
import net.iescierva.ramonmr95.mislugareskotlin2019.casos_uso.CasosUsoLugar
import net.iescierva.ramonmr95.mislugareskotlin2019.datos.Lugares
import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.Lugar

import java.text.DateFormat
import java.util.Date

class VistaLugarActivity : AppCompatActivity() {
    private lateinit var uriUltimaFoto: Uri
    private lateinit var foto: ImageView

    private lateinit var lugares: Lugares
    private lateinit var usoLugar: CasosUsoLugar
    private var pos: Int = 0
    private lateinit var lugar: Lugar

    private val RESULTADO_EDITAR = 1
    private val RESULTADO_GALERIA = 2
    private val RESULTADO_FOTO = 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vista_lugar)
        val extras = intent.extras
        pos = extras!!.getInt("pos", 0)
        lugares = (application as Aplicacion).lugares
        usoLugar = CasosUsoLugar(this, lugares)
        lugar = lugares.elemento(pos)
        actualizaVistas()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.vista_lugar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.accion_compartir -> {
                usoLugar.compartir(lugar)
                return true
            }
            R.id.accion_llegar -> {
                usoLugar.verMapa(lugar)
                return true
            }
            R.id.accion_editar -> {
                val intent = Intent(this, EdicionLugarActivity::class.java)
                intent.putExtra("id", pos)
                intent.putExtra("lugar", lugar)
                startActivityForResult(intent, RESULTADO_EDITAR)
                return true
            }
            R.id.accion_borrar -> {
                usoLugar.borrar(pos)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun verMapa(view: View) {
        usoLugar.verMapa(lugar)
    }

    fun llamarTelefono(view: View) {
        usoLugar.llamarTelefono(lugar)
    }

    fun verPgWeb(view: View) {
        usoLugar.verPgWeb(lugar)
    }

    fun ponerDeGaleria(view: View) {
        usoLugar.galeria(RESULTADO_GALERIA)
    }

    fun tomarFoto(view: View) {
        uriUltimaFoto = usoLugar.tomarFoto(RESULTADO_FOTO)!!
    }

    fun eliminarFoto(view: View) {
        usoLugar.ponerFoto(pos, "", foto)
    }

    fun actualizaVistas() {
        foto = findViewById(R.id.foto)
        usoLugar.visualizarFoto(lugar, foto)

        val nombre = findViewById<TextView>(R.id.nombre)
        nombre.text = lugar.nombre
        val tipo = findViewById<TextView>(R.id.tipo)
        tipo.text = lugar.tipo.texto

        if (lugar.direccion.isEmpty()) {
            findViewById<View>(R.id.direccion).visibility = View.GONE
        } else {
            findViewById<View>(R.id.direccion).visibility = View.VISIBLE
            val direccion = findViewById<TextView>(R.id.direccion)
            direccion.text = lugar.direccion
        }

        if (lugar.telefono == 0) {
            findViewById<View>(R.id.telefono).visibility = View.GONE
        } else {
            findViewById<View>(R.id.telefono).visibility = View.VISIBLE
            val telefono = findViewById<TextView>(R.id.telefono)
            telefono.text = lugar.telefono.toString()
        }

        if (lugar.url.isEmpty()) {
            findViewById<View>(R.id.url).visibility = View.GONE
        } else {
            findViewById<View>(R.id.url).visibility = View.VISIBLE
            val url = findViewById<TextView>(R.id.url)
            url.text = lugar.url
        }

        if (lugar.comentario.isEmpty()) {
            findViewById<View>(R.id.comentario).visibility = View.GONE
        } else {
            findViewById<View>(R.id.url).visibility = View.VISIBLE
            val comentario = findViewById<TextView>(R.id.comentario)
            comentario.text = lugar.comentario
        }

        val fecha = findViewById<TextView>(R.id.fecha)
        fecha.text = DateFormat.getDateInstance().format(
                Date(lugar.fecha))

        val hora = findViewById<TextView>(R.id.hora)
        hora.text = DateFormat.getTimeInstance().format(
                Date(lugar.fecha))

        val valoracion = findViewById<RatingBar>(R.id.valoracion)
        valoracion.rating = lugar.valoracion
        valoracion.onRatingBarChangeListener = RatingBar.OnRatingBarChangeListener { ratingBar, valor, fromUser -> lugar.valoracion = valor }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULTADO_EDITAR) {
            lugar = lugares.elemento(pos)
            actualizaVistas()
            findViewById<View>(R.id.scrollView1).invalidate()
        } else if (requestCode == RESULTADO_GALERIA) {
            if (resultCode == Activity.RESULT_OK) {
                usoLugar.ponerFoto(pos, uriUltimaFoto.toString(), foto)
            } else {
                Toast.makeText(this, "Foto no cargada", Toast.LENGTH_LONG).show()
            }
        } else if (requestCode == RESULTADO_FOTO) {
            if (resultCode == Activity.RESULT_OK) {
                lugar.foto = uriUltimaFoto.toString()
                usoLugar.ponerFoto(pos, lugar.foto!!, foto)
            } else {
                Toast.makeText(this, "Error en captura", Toast.LENGTH_LONG).show()
            }
        }
    }

}


