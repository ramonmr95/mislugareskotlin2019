package net.iescierva.ramonmr95.mislugareskotlin2019.casos_uso

import android.app.Activity
import android.content.Intent
import android.view.View

import net.iescierva.ramonmr95.mislugareskotlin2019.presentacion.AcercaDeActivity
import net.iescierva.ramonmr95.mislugareskotlin2019.presentacion.PreferenciasActivity
import net.iescierva.ramonmr95.mislugareskotlin2019.presentacion.VistaLugarActivity

class CasosUsoActividades(private val actividad: Activity) {

    fun lanzarAcercaDe(view: View? = null) {
        val i = Intent(actividad, AcercaDeActivity::class.java)
        actividad.startActivity(i)
    }

    fun lanzarPreferencias(view: View? = null) {
        val i = Intent(actividad, PreferenciasActivity::class.java)
        actividad.startActivity(i)
    }

    fun lanzarVistaLugarActividad(view: View? = null) {
        val i = Intent(actividad, VistaLugarActivity::class.java)
        actividad.startActivity(i)
    }

}
