package net.iescierva.ramonmr95.mislugareskotlin2019.presentacion

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner

import androidx.appcompat.app.AppCompatActivity

import net.iescierva.ramonmr95.mislugareskotlin2019.Aplicacion
import net.iescierva.ramonmr95.mislugareskotlin2019.R
import net.iescierva.ramonmr95.mislugareskotlin2019.casos_uso.CasosUsoLugar
import net.iescierva.ramonmr95.mislugareskotlin2019.datos.Lugares
import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.Lugar
import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.TipoLugar


class EdicionLugarActivity : AppCompatActivity() {

    private lateinit var lugares: Lugares
    private lateinit var usoLugar: CasosUsoLugar
    private var pos: Int = 0
    private lateinit var lugar: Lugar
    private lateinit var nombre: EditText
    private lateinit var tipo: Spinner
    private lateinit var direccion: EditText
    private lateinit var telefono: EditText
    private lateinit var url: EditText
    private lateinit var comentario: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edicion_lugar)
        val extras = intent.extras
        pos = extras?.getInt("id", 0)?:0
        lugares = (application as Aplicacion).lugares
        lugar = extras?.getParcelable("lugar")?:lugares.elemento(pos)
        usoLugar = CasosUsoLugar(this, lugares)
        addSpinner()
        actualizaVistas()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_edicion_lugar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.cancelEditButton -> {
                finish()
                guardarEdit()
                return super.onOptionsItemSelected(item)
            }
            R.id.AcceptEditButton -> {
                guardarEdit()
                return super.onOptionsItemSelected(item)
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    fun addSpinner() {
        tipo = findViewById(R.id.tipo)
        val adaptador = ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, TipoLugar.nombres)
        adaptador.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        tipo.adapter = adaptador
        tipo.setSelection(lugar.tipo!!.ordinal)
    }

    fun actualizaVistas() {
        nombre = findViewById(R.id.NombreEditText)
        nombre.setText(lugar.nombre)

        direccion = findViewById(R.id.direccionEditText)
        direccion.setText(lugar.direccion)

        telefono = findViewById(R.id.telefonoEditText)
        telefono.setText(lugar.telefono.toString())

        url = findViewById(R.id.urlEditText)
        url.setText(lugar.url)

        comentario = findViewById(R.id.comentariosEditText)
        comentario.setText(lugar.comentario)
    }

    fun guardarEdit() {
        lugar.nombre = nombre.text.toString()
        lugar.tipo = TipoLugar.values()[tipo.selectedItemPosition]
        lugar.direccion = direccion.text.toString()
        lugar.telefono = Integer.parseInt(telefono.text.toString())
        lugar.url = url.text.toString()
        lugar.comentario = comentario.text.toString()
        usoLugar.guardar(pos, lugar)
        finish()
    }
}
