package net.iescierva.ramonmr95.mislugareskotlin2019.casos_uso

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.widget.ImageView
import android.widget.Toast

import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider

import net.iescierva.ramonmr95.mislugareskotlin2019.R
import net.iescierva.ramonmr95.mislugareskotlin2019.datos.Lugares
import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.GeoPunto
import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.Lugar
import net.iescierva.ramonmr95.mislugareskotlin2019.presentacion.EdicionLugarActivity
import net.iescierva.ramonmr95.mislugareskotlin2019.presentacion.VistaLugarActivity

import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.net.URL

class CasosUsoLugar(private val actividad: Activity, private val lugares: Lugares) {

    // OPERACIONES BÁSICAS
    fun mostrar(pos: Int) {
        val i = Intent(actividad, VistaLugarActivity::class.java)
        i.putExtra("pos", pos)
        actividad.startActivity(i)
    }

    fun borrar(id: Int) {
        AlertDialog.Builder(actividad)
                .setTitle(R.string.borradolugar_title)
                .setMessage(R.string.borradolugar_sum)

                .setPositiveButton(R.string.borradolugar_aceptar_borrar) { dialog, whichButton ->
                    lugares.borrar(id)
                    actividad.finish()
                }
                .setNegativeButton(R.string.borradolugar_cancelar_borrar, null)
                .show()
    }

    fun guardar(id: Int, nuevoLugar: Lugar) {
        lugares.actualiza(id, nuevoLugar)
    }


    fun editar(pos: Int, codidoSolicitud: Int) {
        val i = Intent(actividad, EdicionLugarActivity::class.java)
        i.putExtra("pos", pos)
        actividad.startActivityForResult(i, codidoSolicitud)
    }

    fun compartir(lugar: Lugar) {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "text/plain"
        i.putExtra(Intent.EXTRA_TEXT,
                lugar.nombre + " - " + lugar.url)
        actividad.startActivity(i)
    }

    fun llamarTelefono(lugar: Lugar) {
        actividad.startActivity(Intent(Intent.ACTION_DIAL,
                Uri.parse("tel:" + lugar.telefono)))
    }

    fun verPgWeb(lugar: Lugar) {
        actividad.startActivity(Intent(Intent.ACTION_VIEW,
                Uri.parse(lugar.url)))
    }

    fun verMapa(lugar: Lugar) {
        val lat = lugar.posicion.getLatitud()
        val lon = lugar.posicion.getLongitud()
        val uri = if (lugar.posicion !== GeoPunto())
            Uri.parse("geo:$lat,$lon")
        else
            Uri.parse("geo:0,0?q=" + lugar.direccion)
        actividad.startActivity(Intent("android.intent.action.VIEW", uri))
    }

    fun galeria(resultadoGaleria: Int) {
        val action: String
        if (Build.VERSION.SDK_INT >= 19) {
            action = Intent.ACTION_OPEN_DOCUMENT
        } else {
            action = Intent.ACTION_PICK
        }
        val intent = Intent(action,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        actividad.startActivityForResult(intent, resultadoGaleria)
    }

    fun ponerFoto(pos: Int, uri: String, imageView: ImageView) {
        val lugar = lugares.elemento(pos)
        lugar.foto = uri
        visualizarFoto(lugar, imageView)
    }

    fun visualizarFoto(lugar: Lugar, imageView: ImageView) {
        if (lugar.foto != null && !lugar.foto!!.isEmpty()) {
            imageView.setImageBitmap(reduceBitmap(actividad, lugar.foto, 1024, 1024))
        } else {
            imageView.setImageBitmap(null)
        }
    }

    fun tomarFoto(codigoSolicitud: Int): Uri? {
        try {
            val uriUltimaFoto: Uri
            val file = File.createTempFile(
                    "img_" + System.currentTimeMillis() / 1000, ".jpg",
                    actividad.getExternalFilesDir(Environment.DIRECTORY_PICTURES))
            if (Build.VERSION.SDK_INT >= 24) {
                uriUltimaFoto = FileProvider.getUriForFile(
                        actividad, "net.iescierva.ramonmr95.mislugareskotlin2019.FileProvider", file)
            } else {
                uriUltimaFoto = Uri.fromFile(file)
            }
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriUltimaFoto)
            actividad.startActivityForResult(intent, codigoSolicitud)
            return uriUltimaFoto
        } catch (ex: IOException) {
            Toast.makeText(actividad, "Error al crear fichero de imagen",
                    Toast.LENGTH_LONG).show()
            return null
        }

    }


    private fun reduceBitmap(contexto: Context, uri: String?,
                             maxAncho: Int, maxAlto: Int): Bitmap? {
        try {
            var input: InputStream? = null
            val u = Uri.parse(uri)
            if (u.scheme == "http" || u.scheme == "https") {
                input = URL(uri).openStream()
            } else {
                input = contexto.contentResolver.openInputStream(u)
            }
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            options.inSampleSize = Math.max(
                    Math.ceil((options.outWidth / maxAncho).toDouble()),
                    Math.ceil((options.outHeight / maxAlto).toDouble())).toInt()
            options.inJustDecodeBounds = false
            return BitmapFactory.decodeStream(input, null, options)
        } catch (e: FileNotFoundException) {
            Toast.makeText(contexto, "Fichero/recurso de imagen no encontrado",
                    Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return null
        } catch (e: IOException) {
            Toast.makeText(contexto, "Error accediendo a imagen",
                    Toast.LENGTH_LONG).show()
            e.printStackTrace()
            return null
        }

    }


}