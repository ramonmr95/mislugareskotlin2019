package net.iescierva.ramonmr95.mislugareskotlin2019.presentacion


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import net.iescierva.ramonmr95.mislugareskotlin2019.R
import net.iescierva.ramonmr95.mislugareskotlin2019.datos.Lugares
import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.Lugar
import net.iescierva.ramonmr95.mislugareskotlin2019.modelo.TipoLugar

class AdaptadorLugares : RecyclerView.Adapter<AdaptadorLugares.ViewHolder> {
    protected var lugares: Lugares
    protected lateinit var inflador: LayoutInflater
    protected lateinit var contexto: Context
    protected lateinit var onClickListener: View.OnClickListener


    constructor(lugares: Lugares) {
        this.lugares = lugares
    }

    constructor(contexto: Context, lugares: Lugares) {
        this.contexto = contexto
        this.lugares = lugares
        inflador = contexto
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }


    //Creamos nuestro ViewHolder, con los tipos de elementos a modificar
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nombre: TextView
        var direccion: TextView
        var foto: ImageView
        var valoracion: RatingBar

        init {
            nombre = itemView.findViewById(R.id.nombre)
            direccion = itemView.findViewById(R.id.direccion)
            foto = itemView.findViewById(R.id.foto)
            valoracion = itemView.findViewById(R.id.valoracion)
        }

        // Personalizamos un ViewHolder a partir de un lugar
        fun personaliza(lugar: Lugar) {
            nombre.text = lugar.nombre
            direccion.text = lugar.direccion
            var id = R.drawable.otros
            when (lugar.tipo) {
                TipoLugar.RESTAURANTE -> id = R.drawable.restaurante
                TipoLugar.BAR -> id = R.drawable.bar
                TipoLugar.COPAS -> id = R.drawable.copas
                TipoLugar.ESPECTACULO -> id = R.drawable.espectaculos
                TipoLugar.HOTEL -> id = R.drawable.hotel
                TipoLugar.COMPRAS -> id = R.drawable.compras
                TipoLugar.EDUCACION -> id = R.drawable.educacion
                TipoLugar.DEPORTE -> id = R.drawable.deporte
                TipoLugar.NATURALEZA -> id = R.drawable.naturaleza
                TipoLugar.GASOLINERA -> id = R.drawable.gasolinera
            }
            foto.setImageResource(id)
            foto.scaleType = ImageView.ScaleType.FIT_END
            valoracion.rating = lugar.valoracion
        }
    }

    // Creamos el ViewHolder con la vista de un elemento sin personalizar
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Inflamos la vista desde el xml
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.elemento_lista, parent, false)
       // v.setOnClickListener(onClickListener)
        return ViewHolder(v)
    }

    // Usando como base el ViewHolder y lo personalizamos
    override fun onBindViewHolder(holder: ViewHolder, posicion: Int) {
        val lugar = lugares.elemento(posicion)
        holder.personaliza(lugar)
    }

    fun setOnItemClickListener(onClickListener: View.OnClickListener) {
        this.onClickListener = onClickListener
    }

    // Indicamos el número de elementos de la lista
    override fun getItemCount(): Int {
        return lugares.tamanyo()
    }
}
