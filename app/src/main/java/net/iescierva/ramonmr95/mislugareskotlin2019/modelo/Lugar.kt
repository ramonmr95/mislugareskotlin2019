package net.iescierva.ramonmr95.mislugareskotlin2019.modelo

import android.os.Parcel
import android.os.Parcelable

import net.iescierva.ramonmr95.mislugareskotlin2019.datos.GeoException


class Lugar : Parcelable {
    lateinit var nombre: String
    lateinit var direccion: String
    var posicion: GeoPunto
    var foto: String? = null
    var telefono: Int = 0
    lateinit var url: String
    lateinit var comentario: String
    var fecha: Long = 0
    var valoracion: Float = 0.toFloat()

    var tipo: TipoLugar

    @Throws(GeoException::class)
    constructor(nombre: String, direccion: String, latitud: Double,
                longitud: Double, tipo: TipoLugar, telefono: Int, url: String, comentario: String,
                valoracion: Int) {
        fecha = System.currentTimeMillis()
        posicion = GeoPunto(latitud, longitud)
        this.nombre = nombre
        this.direccion = direccion
        this.telefono = telefono
        this.url = url
        this.comentario = comentario
        this.valoracion = valoracion.toFloat()
        this.tipo = tipo
    }

    constructor() {
        fecha = System.currentTimeMillis()
        posicion = GeoPunto()
        tipo = TipoLugar.OTROS
    }

    override fun toString(): String {
        return "Lugar{" +
                "nombre='" + nombre + '\''.toString() +
                ", direccion='" + direccion + '\''.toString() +
                ", posicion=" + posicion +
                ", foto='" + foto + '\''.toString() +
                ", telefono=" + telefono +
                ", url='" + url + '\''.toString() +
                ", comentario='" + comentario + '\''.toString() +
                ", fecha=" + fecha +
                ", valoracion=" + valoracion +
                ", tipo=" + tipo +
                '}'.toString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(nombre)
        dest.writeString(direccion)
        dest.writeParcelable(posicion, 0)
        dest.writeSerializable(tipo)
        dest.writeInt(telefono)
        dest.writeString(url)
        dest.writeString(comentario)
        dest.writeLong(fecha)
        dest.writeFloat(valoracion)
    }

    constructor(parcel: Parcel) {
        this.nombre = parcel.readString().orEmpty()
        this.direccion = parcel.readString().orEmpty()
        this.posicion = parcel.readParcelable(GeoPunto::class.java.getClassLoader())!!
        this.tipo = parcel.readSerializable() as TipoLugar
        this.telefono = parcel.readInt()
        this.url = parcel.readString().orEmpty()
        this.comentario = parcel.readString().orEmpty()
        this.fecha = parcel.readLong()
        this.valoracion = parcel.readFloat()
    }

    companion object CREATOR : Parcelable.Creator<Lugar> {
        override fun createFromParcel(parcel: Parcel): Lugar {
            return Lugar(parcel)
        }

        override fun newArray(size: Int): Array<Lugar?> {
            return arrayOfNulls(size)
        }
    }
}


