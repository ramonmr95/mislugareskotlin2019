package net.iescierva.ramonmr95.mislugareskotlin2019.presentacion

import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity


class PreferenciasActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fragmentManager.beginTransaction()
                .replace(android.R.id.content, PreferenciasFragment())
                .commit()
    }
}