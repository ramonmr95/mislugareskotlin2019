package net.iescierva.ramonmr95.mislugareskotlin2019.modelo

import android.os.Parcel
import android.os.Parcelable

import net.iescierva.ramonmr95.mislugareskotlin2019.datos.GeoException


open class GeoPunto : Parcelable {

    private var latitud: Double = 0.toDouble()
    private var longitud: Double = 0.toDouble()

    @Throws(GeoException::class)
    constructor(latitud: Double, longitud: Double) {
        set(latitud, longitud)
    }

    @Throws(GeoException::class)
    constructor(latitud: Int, longitud: Int) : this(latitud / 1e6, longitud / 1e6) {
    }

    constructor() {
        this.latitud = 0.0
        this.longitud = 0.0
    }

    fun distancia(punto: GeoPunto): Double {
        val RADIO_TIERRA = 6371000.0 // en metros
        val dLat = Math.toRadians(latitud - punto.latitud)
        val dLon = Math.toRadians(longitud - punto.longitud)
        val lat1 = Math.toRadians(punto.latitud)
        val lat2 = Math.toRadians(latitud)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) *
                Math.cos(lat1) * Math.cos(lat2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return c * RADIO_TIERRA
    }

    fun getLongitud(): Double {
        return longitud
    }

    @Throws(GeoException::class)
    fun setLongitud(longitud: Double) {
        if (longitudOK(longitud)) {
            this.longitud = longitud
        } else {
            throw GeoException("Excepcion: setLongitud(double): ('$longitud')")
        }

    }

    fun getLatitud(): Double {
        return latitud
    }

    @Throws(GeoException::class)
    fun setLatitud(latitud: Double) {
        if (latitudOK(latitud)) {
            this.latitud = latitud
        } else {
            throw GeoException("Excepcion: setLatitud(double): ('$latitud')")
        }
    }

    @Throws(GeoException::class)
    operator fun set(latitud: Double, longitud: Double) {
        setLatitud(latitud)
        setLongitud(longitud)
    }

    private fun latitudOK(latitud: Double): Boolean {
        return latitud >= -90 && latitud <= 90
    }

    private fun longitudOK(longitud: Double): Boolean {
        return longitud >= -180 && longitud <= 180
    }

    override fun toString(): String {
        return "GeoPunto{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}'.toString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeDouble(latitud)
        dest.writeDouble(longitud)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GeoPunto

        if (latitud != other.latitud) return false
        if (longitud != other.longitud) return false

        return true
    }

    override fun hashCode(): Int {
        var result = latitud.hashCode()
        result = 31 * result + longitud.hashCode()
        return result
    }

    private constructor(parcel: Parcel) {
        latitud = parcel.readDouble()
        longitud = parcel.readDouble()
    }

    companion object CREATOR: Parcelable.Creator<GeoPunto> {
        override fun createFromParcel(parcel: Parcel): GeoPunto {
            return GeoPunto(parcel)
        }

        override fun newArray(size: Int): Array<GeoPunto?> {
            return arrayOfNulls(size)
        }
    }


}

