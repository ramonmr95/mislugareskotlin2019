package net.iescierva.ramonmr95.mislugareskotlin2019.presentacion

import android.os.Bundle
import android.preference.PreferenceFragment

import net.iescierva.ramonmr95.mislugareskotlin2019.R

class PreferenciasFragment : PreferenceFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.preferencias)
    }
}