package net.iescierva.ramonmr95.mislugareskotlin2019.modelo

import android.os.Parcel
import android.os.Parcelable

import net.iescierva.ramonmr95.mislugareskotlin2019.R

enum class TipoLugar constructor(val texto: String, val recurso: Int) : Parcelable {
    OTROS("Otros", R.drawable.otros),
    RESTAURANTE("Restaurante", R.drawable.restaurante),
    BAR("Bar", R.drawable.bar),
    COPAS("Copas", R.drawable.copas),
    ESPECTACULO("Espectáculo", R.drawable.espectaculos),
    HOTEL("Hotel", R.drawable.hotel),
    COMPRAS("Compras", R.drawable.compras),
    EDUCACION("Educación", R.drawable.educacion),
    DEPORTE("Deporte", R.drawable.deporte),
    NATURALEZA("Naturaleza", R.drawable.naturaleza),
    GASOLINERA("Gasolinera", R.drawable.gasolinera);


    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeInt(ordinal)
    }


    companion object CREATOR : Parcelable.Creator<TipoLugar> {
        override fun createFromParcel(parcel: Parcel): TipoLugar {
            return values()[parcel.readInt()]
        }

        override fun newArray(size: Int): Array<TipoLugar?> {
            return arrayOfNulls(size)
        }

        val nombres: Array<String?>
            get() {
                val resultado = arrayOfNulls<String>(values().size)
                for (tipo in values()) {
                    resultado[tipo.ordinal] = tipo.texto
                }
                return resultado
            }
    }

}

